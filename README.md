# @dws-contributes/duke-scholars-widgets-react

> Simple react interface with scholars widgets api data

[![pipeline status](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-widgets-react/badges/production/pipeline.svg)](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-widgets-react/-/commits/production) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

[![coverage report](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-widgets-react/badges/production/coverage.svg)](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-widgets-react/-/commits/production)

## Install

```bash
npm install --save @dws-contributes/duke-scholars-widgets-react
```

## Usage

All components recieve the same props

### component props

| LABEL        | TYPE                 | NOTES                                     |
| ------------ | -------------------- | ----------------------------------------- |
| uri          | string               | any valid Scholars@duke person uri        |
| displayLabel | boolean              | (optional) true or false                  |
| wrapper      | boolean or component | (optional) this can be a react component  |
| count        | integer              | (optional) the number of items to display |

```jsx
import React, { Component } from 'react'

import { Awards, Courses, Educations, Publications, Grants, Newsfeeds, Overview } from '@dws-contributes/duke-scholars-widgets-react'

const ExampleItem {
  return (
    <Overview
      displayLabel={false}
      uri={'https://scholars.duke.edu/individual/per3175182'}
      wrapper={'div'}
      count={5} />
  )
}
```

compicated example

```jsx
const CustomWrappedWidgets = () => {
  const componentWrapper = ({ children }) => (
    <div className='component-wrapper'>{children}</div>
  )

  const componentsList = [
    Awards,
    Courses,
    Educations,
    Grants,
    Newsfeeds,
    Overview,
    Publications
  ]

  const testUri = 'https://scholars.duke.edu/individual/per3175182'

  return (
    <div>
      {componentsList.map((item) => (
        <Item uri={testUri} wrapper={componentWrapper} />
      ))}
    </div>
  )
}
```

## Local development

clone this repository locally, and run `npm install && npm run-script start`. Then navigate to the `example` directory and run `npm install && npm run-script start`. You can then see this package running at localhost:3000.

## Updating this package

this project uses `semantic versioning` and analyzes the commit messages whether or not a new release should be generated. `fix` in the commit message will trigger a patch release. `add` will trigger an api level change, whereas `breaking` will trigger a version change.

## License

MIT © [DWS](https://webservices.duke.edu)
