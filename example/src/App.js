import '@dws-contributes/duke-scholars-widgets-react/dist/index.css'

import { Awards, Courses, Educations, Grants, Newsfeeds, Overview, Publications } from '@dws-contributes/duke-scholars-widgets-react'

import React from 'react'

const ComponentWrapper = ({displayLabel, children}) => (
  <div className="component-wrapper">
    <h1>{displayLabel}</h1>
    {children}
  </div>
)

const App = () => {
  const testUri = 'https://scholars.duke.edu/individual/per8136422'
  const secondUri = 'https://scholars.duke.edu/individual/per4510902'
  const componentsList = [Awards, Courses, Educations, Grants, Newsfeeds, Overview, Publications]
  return (
    <React.Fragment>
      <div className="test-grouping">
        <h1>Non wrapped components: {testUri}</h1>
        {componentsList.map((Item, index) => (<Item key={"testurinonwrapped-" + index} uri={testUri} displayLabel={true} count={5} numberToDisplay={5} />))}
      </div>
      <div className="test-grouping">
        <h1>wrapped components: {testUri}</h1>
        {componentsList.map((Item, index) => (<Item key={"testuriwrapped-" + index} uri={testUri} displayLabel={false} wrapper={ComponentWrapper} count={5} numberToDisplay={5} />))}
      </div>
      <div className="test-grouping">
        <h1>non wrapped components: {secondUri}</h1>
        {componentsList.map((Item, index) => (<Item key={"seconurinonwrapped-" + index} uri={secondUri} displayLabel={true} count={5} numberToDisplay={5} />))}
      </div>
      <div className="test-grouping">
        <h1>`em` wrapped components: {secondUri}</h1>
        {componentsList.map((Item, index) => (<Item key={"seconduriwrapped-" + index} uri={secondUri} displayLabel={true} wrapper={'em'} count={5} numberToDisplay={5} />))}
      </div>
    </React.Fragment>
  )
}

export default App
