import Awards from './components/Awards'
import Courses from './components/Courses'
import Educations from './components/Educations'
import Fetcher from './Fetcher'
import Grants from './components/Grants'
import Newsfeeds from './components/Newsfeeds'
import Overview from './components/Overview'
import Publications from './components/Publications'

export {
  Fetcher,
  Awards,
  Courses,
  Educations,
  Grants,
  Newsfeeds,
  Overview,
  Publications
}
