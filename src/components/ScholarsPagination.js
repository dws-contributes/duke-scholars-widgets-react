import { Pagination } from 'react-bootstrap'
import React from 'react'

const ScholarsPagination = ({
  page = 0,
  pages = 1,
  numLinks = 9,
  onClick = false
}) => {
  let firstLink = 1
  let lastLink = pages

  if (pages > numLinks) {
    const delta = Math.floor(numLinks / 2)
    const lowerBound = page - delta
    const upperBound = page + delta
    if (upperBound > pages) {
      firstLink = pages - numLinks + 1
      lastLink = pages
    } else {
      firstLink = lowerBound > 0 ? lowerBound : 1
      lastLink = lowerBound > 0 ? upperBound : numLinks
    }
  }

  const previousLink = page > 1 ? page - 1 : null
  const nextLink = page < lastLink ? page + 1 : null

  const pageList = []
  for (let i = firstLink; i <= lastLink; i++) {
    pageList.push(i)
  }

  return (
    <Pagination>
      {previousLink && (
        <Pagination.Prev onClick={() => onClick(previousLink)} />
      )}
      {firstLink > 1 && <Pagination.Ellipsis />}
      {pageList.map((l, i) => (
        <Pagination.Item
          key={page}
          onClick={() => onClick()}
          active={page === i}
        >
          {l}
        </Pagination.Item>
      ))}
      {lastLink < pages && <Pagination.Ellipsis />}
      {nextLink && <Pagination.Next onClick={() => onClick(nextLink)} />}
    </Pagination>
  )
}

export default ScholarsPagination
