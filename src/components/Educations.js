import PropTypes from 'prop-types'
import React from 'react'
import styles from '../styles.module.css'
import useDataFetching from './useDataFetching'
const Educations = ({
  uri,
  displayLabel = true,
  count,
  wrapper,
  numberToDisplay
}) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'educations',
    count
  })

  function _handleClick(event) {
    event.target.nextElementSibling.classList.toggle(
      styles.initiallyHiddenRevealed
    )
  }

  if (data && data.length > 0) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'Education' }
      }
    } else {
      Wrap = React.Fragment
    }

    let initialData = data.reverse()
    let secondaryData = []

    if (numberToDisplay && numberToDisplay < data.length) {
      initialData = data.slice(0, numberToDisplay - 1)
      secondaryData = data.slice(numberToDisplay - 1)
    }

    return (
      <Wrap>
        <React.Fragment>
          <div className='mb-sm-3'>
            {displayLabel && <h5>Education</h5>}
            {data &&
              initialData.map((item, index) => (
                <span className='education' key={index}>
                  {' '}
                  {item.attributes.degree ? item.attributes.degree : item.label}, {item.attributes.institution} (
                  {new Date(item.attributes.endDate).getFullYear()})
                </span>
              ))}
            {secondaryData && secondaryData.length > 0 && (
              <div>
                <button className='reveal-more' onClick={_handleClick}>
                  Load More Education Items
                </button>
                <div className={styles.initiallyHidden}>
                  {secondaryData.map(function (item, index) {
                    return (
                      <span className='education' key={index}>
                        {' '}
                        {item.attributes.degree ? item.attributes.degree : item.label}, {item.attributes.institution}{' '}
                        ({new Date(item.attributes.endDate).getFullYear()})
                      </span>
                    )
                  })}
                </div>
              </div>
            )}
          </div>
        </React.Fragment>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Educations

Educations.propTypes = {
  uri: PropTypes.string,
  displayLabel: PropTypes.bool,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Educations.defaultProps = {
  wrapper: null,
  count: 'all'
}
