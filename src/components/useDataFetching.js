import { useEffect, useState } from 'react'

import Fetcher from '../Fetcher'

function useDataFetching({ uri, count, type, subType }) {
  const [data, setData] = useState([])
  useEffect(() => {
    const fetcher = new Fetcher(uri, type, subType, count)
    fetcher.fetchData().then((data) => setData(data))
  }, [uri, count])

  return {
    data
  }
}

export default useDataFetching
