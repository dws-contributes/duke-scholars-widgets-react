import PropTypes from 'prop-types'
import React from 'react'
import parse from 'html-react-parser'
import useDataFetching from './useDataFetching'

const Overview = ({ uri, displayLabel = true, wrapper }) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'overview',
    count: 'all'
  })

  if (data && data.length > 0 && data[0]?.overview) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'Overview' }
      }
    } else {
      Wrap = React.Fragment
    }
    return (
      <Wrap>
        <div className='mb-sm-3'>
          {displayLabel && <h5>Overview</h5>}
          {data.map((item) => parse(item.overview))}
        </div>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Overview

Overview.propTypes = {
  uri: PropTypes.string,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  displayLabel: PropTypes.bool
}
