import PropTypes from 'prop-types'
import React from 'react'
import styles from '../styles.module.css'
import useDataFetching from './useDataFetching'
const Awards = ({ uri, displayLabel, count, wrapper, numberToDisplay }) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'awards',
    count
  })

  function _handleClick(event) {
    event.target.nextElementSibling.classList.toggle(
      styles.initiallyHiddenRevealed
    )
  }

  if (data && data.length > 0) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'Awards & Honors' }
      }
    } else {
      Wrap = React.Fragment
    }

    let initialData = data
    let secondaryData = []

    if (numberToDisplay && numberToDisplay < data.length) {
      initialData = data.slice(0, numberToDisplay - 1)
      secondaryData = data.slice(numberToDisplay - 1)
    }
    return (
      <Wrap>
        <React.Fragment>
          {displayLabel && <h2>Awards & Honors</h2>}
          <ul>
            {data.length > 0 &&
              initialData.map(function (item, index) {
                return (
                  <li key={index}>
                    <a href={item.uri}>
                      {item.label} -{' '}
                      {new Date(item.attributes.date).getFullYear()}
                    </a>
                  </li>
                )
              })}
          </ul>
          {secondaryData && secondaryData.length > 0 && (
            <div>
              <button className='reveal-more' onClick={_handleClick}>
                Load More Awards
              </button>
              <div className={styles.initiallyHidden}>
                <ul>
                  {secondaryData.map(function (item, index) {
                    return (
                      <li key={index}>
                        <a href={item.uri}>
                          {item.label} -{' '}
                          {new Date(item.attributes.date).getFullYear()}
                        </a>
                      </li>
                    )
                  })}
                </ul>
              </div>
            </div>
          )}
        </React.Fragment>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Awards

Awards.propTypes = {
  uri: PropTypes.string,
  displayLabel: PropTypes.bool,
  data: PropTypes.object,
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Awards.defaultProps = {
  wrapper: null,
  count: 'all'
}
