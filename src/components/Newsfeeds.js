import PropTypes from 'prop-types'
import React from 'react'
import styles from '../styles.module.css'
import useDataFetching from './useDataFetching'

const Newsfeeds = ({
  uri,
  displayLabel = true,
  count,
  wrapper,
  numberToDisplay
}) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'newsfeeds',
    count
  })

  function _handleClick(event) {
    event.target.nextElementSibling.classList.toggle(
      styles.initiallyHiddenRevealed
    )
  }

  if (data && data.length > 0) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'In The News' }
      }
    } else {
      Wrap = React.Fragment
    }

    let initialData = data
    let secondaryData = []

    if (numberToDisplay && numberToDisplay < data.length) {
      initialData = data.slice(0, numberToDisplay - 1)
      secondaryData = data.slice(numberToDisplay - 1)
    }

    return (
      <Wrap>
        <React.Fragment>
          {displayLabel && <h5>In The News</h5>}
          <ul>
            {data.length > 0 &&
              initialData.map(function (item, index) {
                return (
                  <li key={index}>
                    <a href={item.attributes.newsLink}>
                      {item.label} - {item.attributes.newsMonth}/
                      {item.attributes.newsDay}/{item.attributes.newsYear}
                    </a>
                  </li>
                )
              })}
          </ul>
          {secondaryData && secondaryData.length > 0 && (
            <div>
              <button className='reveal-more' onClick={_handleClick}>
                Load More News Items
              </button>
              <div className={styles.initiallyHidden}>
                <ul>
                  {secondaryData.map(function (item, index) {
                    return (
                      <li key={index}>
                        <a href={item.attributes.newsLink}>
                          {item.label} - {item.attributes.newsMonth}/
                          {item.attributes.newsDay}/{item.attributes.newsYear}
                        </a>
                      </li>
                    )
                  })}
                </ul>
              </div>
            </div>
          )}
        </React.Fragment>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Newsfeeds

Newsfeeds.propTypes = {
  uri: PropTypes.string,
  displayLabel: PropTypes.bool,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Newsfeeds.defaultProps = {
  wrapper: null,
  count: 'all',
  numberToDisplay: 0
}
