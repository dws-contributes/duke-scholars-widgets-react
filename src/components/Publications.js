import PropTypes from 'prop-types'
import React from 'react'
import parse from 'html-react-parser'
import styles from '../styles.module.css'
import useDataFetching from './useDataFetching'

const Publications = ({
  uri,
  displayLabel = true,
  count,
  wrapper,
  numberToDisplay
}) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'publications',
    count
  })

  function _handleClick(event) {
    event.target.nextElementSibling.classList.toggle(
      styles.initiallyHiddenRevealed
    )
  }

  if (data && data.length > 0) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'Selected Publications' }
      }
    } else {
      Wrap = React.Fragment
    }

    let initialData = data
    let secondaryData = []

    if (numberToDisplay && numberToDisplay < data.length) {
      initialData = data.slice(0, numberToDisplay - 1)
      secondaryData = data.slice(numberToDisplay - 1)
    }

    return (
      <Wrap>
        <React.Fragment>
          {displayLabel && <h5>Selected Publications</h5>}
          {initialData.map(function (item, index) {
            return (
              <div key={index} className='mb-sm-3'>
                {parse(item.attributes.chicagoCitation)}
              </div>
            )
          })}
          {secondaryData && secondaryData.length > 0 && (
            <div>
              <button className='reveal-more' onClick={_handleClick}>
                Load More Publications
              </button>
              <div className={styles.initiallyHidden}>
                {secondaryData.map(function (item, index) {
                  return (
                    <div key={index} className='mb-sm-3'>
                      {parse(item.attributes.chicagoCitation)}
                    </div>
                  )
                })}
              </div>
            </div>
          )}
        </React.Fragment>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Publications

Publications.propTypes = {
  uri: PropTypes.string,
  displayLabel: PropTypes.bool,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  numberToDisplay: PropTypes.number
}

Publications.defaultProps = {
  wrapper: null,
  count: 'all',
  numberToDisplay: 0
}
