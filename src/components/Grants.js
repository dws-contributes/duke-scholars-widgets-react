import PropTypes from 'prop-types'
import React from 'react'
import styles from '../styles.module.css'
import useDataFetching from './useDataFetching'

const Grants = ({
  uri,
  displayLabel = true,
  count,
  wrapper,
  numberToDisplay
}) => {
  const { data } = useDataFetching({
    uri,
    type: 'people',
    subType: 'grants',
    count
  })

  function _handleClick(event) {
    event.target.nextElementSibling.classList.toggle(
      styles.initiallyHiddenRevealed
    )
  }

  if (data && data.length > 0) {
    let Wrap
    if (wrapper) {
      Wrap = wrapper
      if (typeof wrapper !== 'string') {
        Wrap.defaultProps = { displayLabel: 'Selected Grants' }
      }
    } else {
      Wrap = React.Fragment
    }

    let initialData = data
    let secondaryData = []

    if (numberToDisplay && numberToDisplay < data.length) {
      initialData = data.slice(0, numberToDisplay - 1)
      secondaryData = data.slice(numberToDisplay - 1)
    }

    return (
      <Wrap>
        <React.Fragment>
          {displayLabel && <h5>Selected Grants</h5>}
          <ul>
            {data.length > 0 &&
              initialData.map(function (item, index) {
                return (
                  <li key={index}>
                    <a href={item.uri}>{item.label}</a>{' '}
                    {new Date(item.attributes.startDate).getFullYear()} -{' '}
                    {new Date(item.attributes.endDate).getFullYear()}
                  </li>
                )
              })}
          </ul>
          {secondaryData && secondaryData.length > 0 && (
            <div>
              <button className='reveal-more' onClick={_handleClick}>
                Load More Grants
              </button>
              <div className={styles.initiallyHidden}>
                <ul>
                  {secondaryData.map(function (item, index) {
                    return (
                      <li key={index}>
                        <a href={item.uri}>{item.label}</a>{' '}
                        {new Date(item.attributes.startDate).getFullYear()} -{' '}
                        {new Date(item.attributes.endDate).getFullYear()}
                      </li>
                    )
                  })}
                </ul>
              </div>
            </div>
          )}
        </React.Fragment>
      </Wrap>
    )
  } else {
    return null
  }
}

export default Grants

Grants.propTypes = {
  uri: PropTypes.string,
  displayLabel: PropTypes.bool,
  wrapper: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

Grants.defaultProps = {
  wrapper: null,
  count: 'all',
  numberToDisplay: 0
}
