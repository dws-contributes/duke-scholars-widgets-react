class Fetcher {
  constructor(uri, type, subType, count) {
    this.uri = uri
    this.type = this.types.includes(type) ? type : 'people'
    this.subType = this.subTypes.includes(subType) ? subType : 'overview'
    this.count = Number.isInteger(count) ? count : 'all'
  }

  data = {}

  endpoint = 'https://scholars.duke.edu/widgets/api/v0.9'

  types = ['people', 'organizations']

  subTypes = [
    'publications',
    'artistic_works',
    'awards',
    'courses',
    'newsfeeds',
    'grants',
    'professional_activities',
    'positions',
    'addresses',
    'overview',
    'contact',
    'educations',
    'webpages',
    'gifts',
    'licenses',
    'academic_positions',
    'geographical_focus',
    'past_appointments',
    'artistic_events',
    'research_areas'
  ]

  async fetchData() {
    const url =
      [this.endpoint, this.type, this.subType, this.count + '.json'].join('/') +
      '?uri=' +
      this.uri
    try {
      const res = await fetch(url)
      return res.json()
    } catch (err) {
      console.log('error', err)
    }
  }
}

export default Fetcher
