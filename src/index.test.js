import {
  Awards,
  Courses,
  Educations,
  Grants,
  Newsfeeds,
  Overview,
  Publications
} from '.'

const componentList = {
  awards: Awards,
  courses: Courses,
  educations: Educations,
  grants: Grants,
  newsfeeds: Newsfeeds,
  overview: Overview,
  publications: Publications
}

Object.keys(componentList).map((label) => {
  describe(label, () => {
    it('is truthy', () => {
      expect(componentList[label]).toBeTruthy()
    })
  })
})
